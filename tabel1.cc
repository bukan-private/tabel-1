#include <iostream>

using namespace std;

int main() {
  int i, j;
  cout << "---------------------------" << endl;
  cout << "Masukkan nilai i: "; cin >> i;
  cout << "Masukkan nilai j: "; cin >> j;

  cout << "| operasi | hasil operasi |" << endl;
  cout << "----------+----------------" << endl;
  cout << i << " + " << j << "     | " << i + j << "             |" << endl;
  cout << i << " - " << j << "     | " << i - j << "            |" << endl;
  cout << i << " * " << j << "     | " << i * j << "             |" << endl;
  cout << i << " div " << j << "   | " << i / j << "             |" << endl;
  cout << i << " mod " << j << "   | " << i % j << "             |" << endl;
  cout << "---------------------------" << endl;
  
  return 0;
}
